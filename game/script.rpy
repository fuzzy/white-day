﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define s = Character("Seeder-kun")
define k = Character("KK")
define i = Character("Inventory")

image seeder = "seeder.gif"
image KK = "kk.png"
image meds = "meds.jpg"
image present = "present.png"
image Komugi = "komugi.png"

# The game starts here.

label start:

  scene bedroom at default:
    zoom 0.43

  s "Today is the day!"
  s "Today I will reciprocate KK's feelings by giving her this amazing white day present!"
  show present at truecenter:
    zoom 0.33
  i "Item Acquired: Present"
  hide present

  s "Actually, maybe I should clean my room first..."

  label clean_choice:
  menu:
    "Should I clean my room?"

    "Yes":
      jump clean_choice

    "No":
      s "No, of course I shouldn't! I don't have time to anyways!"
      s "I have to find KK to give her the present!"
      jump bedroom_scene

  label bedroom_scene:
  scene bedroom at default:
    zoom 0.43
  menu:
    "Where should I look?"
    "Look around bedroom":
      s "KK doesn't seem to be in my bedroom unfortunately... at least I have my figurines! :3"
      jump bedroom_scene
    "Go to hallway":
      jump hallway_scene

  label hallway_scene:
  scene hallway at default:
    zoom 0.7
  menu:
    "Where should I look?"

    "Go to Bedroom":
      jump bedroom_scene
    "Go to Guestroom":
      jump guestroom_scene
    "Go to Bathroom":
      jump bathroom_scene
    "Go to Living Room":
      jump living_scene

  label guestroom_scene:
  scene bedroom2 at truecenter:
    zoom 0.7
  menu:
    "Where should I look?"
  
    "Look around Guestroom":
      s "I don't see KK in here... but there is my computer! I even set up a second one so we can play our favourite games together! :3"
      jump guestroom_scene
    "Return to Hallway":
      jump hallway_scene

  label bathroom_scene:
  scene bathroom at default:
    zoom 0.45
  menu:
    "Where should I look?"

    "Look around Bathroom":
      "KK isn't in here... she'd probably call me a pervert if she was! What a shame heheh... Oh, while I'm in here I can take my medication..."
      jump meds_menu

    "Return to Hallway":
      jump hallway_scene
  
  label meds_menu:
  show meds at truecenter
  "There is a bottle of Aripiprazole in front of you."
  hide meds
  menu:
    s "Should I take my meds?"

    "Take meds":
      jump meds_yes

    "Don't take meds":
      "No, I don't have time to waste on this. I need to find my love, KK Cyber!"
      hide meds
      jump bathroom_scene

  label living_scene:
  scene livingroom at default:
    zoom 0.5
  menu:
    "Where should I look?"

    "Look around Living Room":
      s "It's my living room! One day I'll get a big screen TV to watch anime on with KK!"
      jump living_scene
    "Kitchen":
      jump kitchen_scene
    "Hallway":
      jump hallway_scene

  label kitchen_scene:
  scene kitchen at default:
    zoom 0.5
  menu:
    "Look around Kitchen":
      s "This is where I make the meals I eat while watching KK stream! I have dreams of eating ramen with KK in here... ehe~"
      jump kitchen_scene
    "Foyer":
      jump foyer_scene
    "Living Room":
      jump living_scene

  label foyer_scene:
  scene foyer at truecenter:
    zoom 2

  menu:
    "Where should I look?"

    "Look around Foyer":
      s "This is my foyer! It's my favourite room in the house."
      s "It's just so cozy!"
      s "Hmmm I don't see KK around here..."
      s "Wait! Under this used bandage!"
      show KK at right:
        zoom 1.1
        xoffset -200
      show seeder at left:
        zoom 1.5
        xoffset 200
      s "KK! It's really you! I'm so Happy!"
      s "I really wanted to give you a present for White Day... I spent a long time thinking about what to give you"
      s "Please accept this!"
      show present at truecenter:
        zoom 0.33
      i "Present removed from Inventory"
      hide present
      k "W-wow Seeder-kun!"
      k "I love it! Thank you so much!"
      hide KK
      hide seeder
      with dissolve
      show Komugi at truecenter
      with dissolve
      ""
      hide Komugi
      show KK at right:
        zoom 1.1
        xoffset -200
      show seeder at left:
        zoom 1.5
        xoffset 200
      with dissolve
      k "And I love you, Seeder-kun! Let's be together forever!!!!"
      k "And ever and ever and ever and ever and ever and ever and ever and ever and ever!!!!!!!"
      "The end!"
      jump credits_scene

    "Return to Kitchen":
      jump kitchen_scene
      

  label meds_yes:
    scene mirror
    
    "You place the medication into your mouth and take a sip of water"
    s "What's going on? I don't feel so good..."
    show KK at left:
      zoom 2
      yoffset 540
    k "Seeder-kun."
    k "What did I tell you about taking your meds?"
    s "KK? Is that you?"
    s "No, you're not real... My therapist said you're not real..."
    k "But I'm right here, Seeder-kun?"
    k "And I'm going to give you all of my love."
    k "But you've been bad, haven't you?"
    k "You know you're only supposed to take my medicine."
    s "I-I'm sorry... "
    k "You know I don't want to do this, but you leave me no other choice."
    show KK at default:
      zoom 6
      yoffset 3500
    k "Oh? Who's this? You have a friend with you?"
    k "Sorry, but me and Seeder-kun need some privacy."
    k "Get out."
    jump crash_scene

  label credits_scene:
    scene credits
    "Thank you for all of your hard work KK!"
    "And happy white day!!!!!!!!!!!"
    "Aishiteru!!!!!!!!!!!!!!!!!!!!!!!!"
    "Aishiteruaishiteruaishiteruaishiteruaishiteruaishiteruaishiteruaishiteruaishiteru"
    "aishiteruaishiteruaishiteruaishiteruaishiteruaishiteruaishiteruaishiteruaishiteru"
    "aishiteruaishiteruaishiteruaishiteruaishiteruaishiteruaishiteruaishiteru"
    "aishiteruaishiteruaishiteruaishiteruaishiteruaishiteruaishiteruaishiteruaishiteru"
    "aishiteruaishiteruaishiteruaishiteruaishiteru!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!111"
    "Don't forget to take your meds tehepero~!"
    jump game_end

label crash_scene:
$ renpy.quit()

label game_end:
return
